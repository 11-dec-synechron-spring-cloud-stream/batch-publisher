package com.classpath.batchpublishing.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;
import java.util.stream.Collectors;

@Configuration
public class ApplicationConfiguration {

    private  AtomicInteger counter = new AtomicInteger(0);

    @Bean
    public Function<String, List<String>> batchPublisher(){
            return (value) -> {
                final List<String> collection = Arrays.asList(value.toUpperCase(), value.toLowerCase());
                return collection;
            };
    }
    @Bean
    public Function<List<String>, List<String>> batchListFunction(){
            return (value) -> {
                System.out.println("Batch size :: " +value.size());
                return value.stream().map(String::toUpperCase).collect(Collectors.toList());
            };
    }
}