package com.classpath.batchpublishing;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BatchPublishingApplication {

    public static void main(String[] args) {
        SpringApplication.run(BatchPublishingApplication.class, args);
    }

}
